##################################################################################
# VARIABLES
##################################################################################

# HTTP Settings

#http_directory = "http"
floppy_files = [
  "./answer_files/autounattend.xml",
  "./scripts/install-vm-tools.cmd",
  "./scripts/enable-rdp.cmd",
  "./scripts/win-updates.ps1",
  "./scripts/disable-network-discovery.cmd",
  "./scripts/disable-winrm.ps1",
  "./scripts/enable-winrm.ps1"
]
# Virtual Machine Settings
vm_boot_command             = [
]

vm_name                     = "Windows Server 2022 Template"
vm_guest_os_type            = "windows9Server64Guest"
vm_version                  = 14
vm_firmware                 = "bios"
vm_cdrom_type               = "sata"
vm_cpu_sockets              = 4
vm_cpu_cores                = 1
vm_mem_size                 = 8192
vm_disk_size                = 64000
vm_disk_controller_type     = ["pvscsi"]
vm_network_card             = "vmxnet3"
vm_boot_wait                = "2s"

# ISO Objects

iso_paths                   = ["[HPDS01] .ISOs/Windows/WindowsServer2022.iso", "[] /usr/lib/vmware/isoimages/windows.iso"]
iso_checksum                = "4f1457c4fe14ce48c9b2324924f33ca4f0470475e6da851b39ccbf98f44e7852"
iso_urls                    = ["https://software-download.microsoft.com/download/sg/20348.169.210806-2348.fe_release_svc_refresh_SERVER_EVAL_x64FRE_en-us.iso", "./vmtools/windows.iso"]
